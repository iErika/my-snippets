'use strict';

// ES6 uglify
const uglifyes = require('uglify-es');
const composer = require('gulp-uglify/composer');
const uglify = composer(uglifyes, console);
// Misc
const rename = require('gulp-rename');
const path = require('path');


const srcDir = path.join('public', 'assets');

gulp.task("compressScripts", () => {
    return gulp.src([
        `${srcDir}/js/*.js`,
        `${srcDir}/js/**/*.js`,
    ])
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest(`${srcDir}/js`));
});
